const fs = require('fs');
const axios = require('axios');

const gitlabToken = process.env.GITLAB_TOKEN;
const groupID = process.env.GROUP_ID;

async function fetchGroupMembers() {
    try {
        const internalUsers = [];
        const externalUsers = [];
        let page = 1;

        while (true) {
            const response = await axios.get(`https://gitlab.com/api/v4/groups/${groupID}/members`, {
                params: {
                    private_token: gitlabToken,
                    per_page: 100,
                    page: page++
                }
            });

            const members = response.data;

            if (members.length === 0) {
                break;
            }

            for (const member of members) {
                const userResponse = await axios.get(`https://gitlab.com/api/v4/users/${member.id}`, {
                    params: {
                        private_token: gitlabToken
                    }
                });

                const user = userResponse.data;

                if (user.external) {
                    externalUsers.push(user);
                } else {
                    internalUsers.push(user);
                }
            }
        }

        return { internalUsers, externalUsers };
    } catch (error) {
        console.error('Error fetching group members:', error);
        throw error;
    }
}

async function generateHTMLReport() {
    try {
        const { internalUsers, externalUsers } = await fetchGroupMembers();

        const htmlContent = `
            <!DOCTYPE html>
            <html>
            <head>
                <title>User Report</title>
            </head>
            <body>
                <h1>Internal Users</h1>
                <ul>
                    ${internalUsers.map(user => `<li>${user.username} (${user.email})</li>`).join('')}
                </ul>
                <h1>External Users</h1>
                <ul>
                    ${externalUsers.map(user => `<li>${user.username} (${user.email})</li>`).join('')}
                </ul>
            </body>
            </html>
        `;

        fs.writeFileSync('users.html', htmlContent);
        console.log('HTML report generated successfully!');
    } catch (error) {
        console.error('Error generating HTML report:', error);
    }
}

generateHTMLReport();
